// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/main.css'
import axios from "axios";
import VideoPlayer from 'vue-video-player'
import common from "./components/common";

Vue.use(VideoPlayer)

require('vue-video-player/src/custom-theme.css')

Vue.config.productionTip = false
Vue.use(ElementUI);

axios.defaults.baseURL = 'api/'
Vue.config.$axios = axios
/* eslint-disable no-new */
router.beforeEach((to, from, next)=>{
  if (to.matched.some(function (item) {
    if (sessionStorage.getItem("userInfo") === null){
      sessionStorage.setItem("userInfo", JSON.stringify(common.defaultUserInfo))
    }
    let userInfo = JSON.parse(sessionStorage.getItem("userInfo"))
    if (userInfo.uid !== -1) return false
    return item.meta.requireLogin
  })){
    next('/login')
  }else {
    next()
  }
})

new Vue({
  el: '#app',
  router: router,
  components: {
    App
  },
  template: '<App/>'
})
