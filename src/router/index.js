import Vue from 'vue'
import Router from 'vue-router'
// 布局组件
import Main from '../components/Main.vue'
import Login from '../components/login.vue'
import Register from '../components/register'
import Show from '../components/show.vue'
import User from "../components/user.vue"

import Favorites from "../components/favorites.vue"
import Contribution from "../components/contribution.vue"
import Dynamic from "../components/dynamic.vue"

Vue.use(Router)

export default new Router({
  routes: [ //路由就是管理跳转路径的
    //播放界面
    {
      path: '/show',
      name: 'show',
      component: Show,
      meta: {
        requireLogin: true
      }
    },
    //登录界面
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        requireLogin: false
      }
    },
    //注册页面
    {
      path: '/register',
      name: "register",
      component: Register,
      meta: {
        requireLogin: false
      }
    },
    //用户界面
    {
      path:"/user",
      name:"user",
      component:User,
      meta: {
        requireLogin: true
      },
      children:[
        {
          path: '/favorites',
          name:'favorites',
          component:Favorites,
        },
        {
          path: '/contribution',
          name:'contribution',
          component: Contribution,
        },
        {
          path: '/dynamic',
          name:'dynamic',
          component:Dynamic,
        }
      ]
    },

    // 反斜杠重定义
    {
      path: '/',
      redirect: "/main"
    },
    //首页跳转路径
    {
      path: '/main', //跳转路径
      name: 'main', //当前组件名称
      component: Main, //跳转的组件
      meta: {
        requireLogin: false
      },
      children: []
    },
  ]

})
